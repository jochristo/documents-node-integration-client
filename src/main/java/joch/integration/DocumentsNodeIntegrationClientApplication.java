package joch.integration;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.UUID;
import joch.integration.configuration.RabbitMQConfiguration;
import joch.integration.handler.DocumentRequestHandler;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.service.IDocumentService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.SerializationUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Admin
 */
@SpringBootApplication
@EnableRabbit
@EnableScheduling
public class DocumentsNodeIntegrationClientApplication implements CommandLineRunner
{           
    private static final Logger logger = LoggerFactory.getLogger(DocumentRequestHandler.class);
    private final IDocumentService service;    
    //private final RabbitTemplate rabbitTemplate;    
    private final AsyncRabbitTemplate asyncRabbitTemplate;
    
    @Autowired
    public DocumentsNodeIntegrationClientApplication(IDocumentService service , AsyncRabbitTemplate asyncRabbitTemplate) {
        this.service = service;
        this.asyncRabbitTemplate = asyncRabbitTemplate;
    }            
    
    public static void main(String[] args) {
        SpringApplication.run(DocumentsNodeIntegrationClientApplication.class, args);                  
    } 

    @Override
    public void run(String... args) throws Exception
    {        
        logger.info("Running documents-node client...");
        producer();
    }        
    
    //@Scheduled(fixedRate = 2000)    
    public void producer() throws IOException, InterruptedException
    {
        int count = 0;
        for(int i = 0; i<1000; i++){
            count++;
            DocumentUploadParameter parameter = new DocumentUploadParameter();
            parameter.setIntegrity("integrity hash");
            
            // create temp file
            Path dir = FileSystems.getDefault().getPath("C:\\TEMP\\documents-node-integration");
            String prefix = "document_rabbitmq_";
            String suffix=".txt";   
            String randomFilename = UUID.randomUUID().toString();
            StringBuilder sb = new StringBuilder(prefix).append(randomFilename);
            //Path path = Files.createTempFile(dir, sb.toString(), suffix);
                        
            File file;
            file = new File("C:\\TEMP\\integration.txt");               
            //file = path.toFile();
            
            DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length(), file.getParentFile());
            fileItem.getOutputStream();
            MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
            parameter.setFile(multipartFile);        
            parameter.setIntegrity(DigestUtils.sha256Hex(fileItem.get()));
                    
            byte[] data = SerializationUtils.serialize(parameter);            
            //MessageProperties properties = MessagePropertiesBuilder.newInstance().setCorrelationId(message.getHeaders().get("amqp_correlationId").toString()).build();                                
            //asyncRabbitTemplate.convertSendAndReceive(RabbitMqConfiguration.DOCUMENT_REQUEST_EXCHANGE, RabbitMqConfiguration.DOCUMENT_REQUEST_QUEUE, MessageBuilder.withBody(data).build());
            asyncRabbitTemplate.sendAndReceive(RabbitMQConfiguration.DOCUMENTS_NODE_EXCHANGE_NAME, RabbitMQConfiguration.DOCUMENTS_NODE_ROUTING_KEY_NAME, MessageBuilder.withBody(data).build());
            logger.info("Rabbit is sending message with body: " + parameter.toString());                  
            if(count == 100) Thread.sleep(2000);
        }        
    }

  
}
