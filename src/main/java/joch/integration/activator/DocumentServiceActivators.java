package joch.integration.activator;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import joch.integration.handler.DocumentRequestHandler;
import joch.integration.http.Constants;
import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Admin
 */
@Component
public class DocumentServiceActivators
{       
    private static final Logger logger = LoggerFactory.getLogger(DocumentRequestHandler.class);

    @ServiceActivator(inputChannel = "documentRequestChannel", outputChannel = "documentReplyChannel")
    public FileDocument getResponse(Message<String> message) throws URISyntaxException
    {        
        logger.info("Message received on channel: documentRequestChannel");
        MessageChannel replyChannel = (MessageChannel) message.getHeaders().get("replyChannel");
        logger.info("Requesting Document info, replyChannel channel: " + replyChannel.toString());        
        String payload = (String) message.getPayload();
        logger.info("Message payload: " + payload.toString());                      
        logger.info("Contacting external RESTful API: document repository service...");  
        RestTemplate rest = new RestTemplate();        
        FileDocument document = rest.getForObject(new URI(Constants.DOCUMENTS_NODE_URL + payload), FileDocument.class);
        logger.info("FileDocument returned : " + document.toString());  
        return document;
    }

    @ServiceActivator(inputChannel = "documentReplyChannel")
    public FileDocument getDocumentResponse(Message<FileDocument> message) throws URISyntaxException
    {
        logger.info("Message received on channel: documentReplyChannel");        
        FileDocument payload = (FileDocument) message.getPayload();
        logger.info("Message payload: " + payload.toString());                              
        return payload;
    }    
    
    @ServiceActivator(inputChannel = "documentModelChannel")
    public DocumentModel getDocumentModelResponse(Message<DocumentUploadParameter> message) throws URISyntaxException, IOException
    {
        logger.info("Message received on channel: documentModelChannel");        
        DocumentUploadParameter payload = (DocumentUploadParameter) message.getPayload();
        logger.info("Message payload: " + payload.toString());    
        logger.info("Contacting external RESTful API: document repository service...");  
        RestTemplate rest = new RestTemplate();        
        DocumentModel document = rest.postForObject(new URI(Constants.DOCUMENTS_NODE_URL), requestEntity(payload), DocumentModel.class);
        logger.info("DocumentModel returned : " + document.toString());  
        return document;        
    }      
       
    @ServiceActivator(inputChannel = "documentErrorChannel")
    public void handleDocumentErrors(Message<MessageHandlingException> message)
    {
        //DocumentUploadParameter requestedParameter = (DocumentUploadParameter) message.getPayload().getFailedMessage().getPayload();
        logger.info("Error received on channel: documentErrorChannel");   
        Throwable tr = message.getPayload().getCause();
        if(tr instanceof HttpServerErrorException){
            HttpServerErrorException ex = (HttpServerErrorException) tr;
            String body = ex.getResponseBodyAsString();
            logger.info("Specific error: " + body);              
        }                     
        logger.info("Message payload: " + message.getPayload().getMessage());        
    }
    
    private static HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity (DocumentUploadParameter payload) throws IOException
    {        
        File file = new File(payload.getFile().getOriginalFilename());
        payload.getFile().transferTo(file);
        FileSystemResource value = new FileSystemResource(file);
        
        
        String absPath = value.getFile().getAbsolutePath();
        logger.info("file absolute path: " + absPath);
        
        LinkedMultiValueMap<String, Object> multipartRequest = new LinkedMultiValueMap<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        
        // putting the two parts in one request
        multipartRequest.add("integrity", payload.getIntegrity());
        multipartRequest.add("file", value);        
        HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(multipartRequest, headers);        
        return entity;
    }    
    
    

}
