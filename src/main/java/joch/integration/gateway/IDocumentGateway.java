package joch.integration.gateway;

import java.util.concurrent.Future;
import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

/**
 *
 * @author Admin
 */
@MessagingGateway(name = "documentsGateway", errorChannel = "documentErrorChannel")
public interface IDocumentGateway {
    
    @Gateway(requestChannel = "documentModelChannel", replyTimeout=5000)    
    public Future<DocumentModel> uploadAsync(@Payload DocumentUploadParameter parameter);
    
    @Gateway(requestChannel = "documentRequestChannel", replyChannel = "documentReplyChannel", replyTimeout=5000 )
    public FileDocument getDocument(String id);    
    
    @Gateway(requestChannel = "documentRequestChannel", replyChannel = "documentReplyChannel", replyTimeout=5000)
    public Future<FileDocument> getDocumentAsync(String id);      
    
    
}
