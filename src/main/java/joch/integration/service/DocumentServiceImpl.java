/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joch.integration.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import joch.integration.gateway.IDocumentGateway;
import joch.integration.model.DocumentModel;
import joch.integration.model.DocumentUploadParameter;
import joch.integration.model.FileDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class DocumentServiceImpl implements IDocumentService
{
    @Autowired
    private IDocumentGateway gateway;
    
    @Override
    public DocumentModel create(DocumentUploadParameter parameter) {
        try {
            return gateway.uploadAsync(parameter).get();
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(DocumentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return null;
    }
    

    @Override
    public FileDocument getDocument(String id) {
        FileDocument future = gateway.getDocument(id);
        return future;
    }
    
    @Override
    public FileDocument getDocumentAsync(String id)
    {        
        try {
            Future<FileDocument> future  = gateway.getDocumentAsync(id);
            return future.get(2, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException ex) {
            Logger.getLogger(DocumentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }    

    @Override
    public void createDocument(DocumentUploadParameter parameter) {
        gateway.uploadAsync(parameter);
    }


    
    
    
}
