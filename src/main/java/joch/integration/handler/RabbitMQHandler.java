package joch.integration.handler;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import joch.integration.configuration.RabbitMQConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Admin
 */
@Service
public class RabbitMQHandler
{   
    private static final Logger logger = LoggerFactory.getLogger(RabbitMQHandler.class);
    
    private Date startup;
    private Date last;
    private long counter = 0;
    
    /*
    @RabbitListener(queues = RabbitMqConfiguration.DOCUMENT_REQUEST_QUEUE)
    public void handleDocumentRequestMessage(Message<?> message) throws MessagingException, InterruptedException, ExecutionException
    {
        logger.info("Rabbit listened message: " + message.getPayload().toString());
        // read message
        DocumentUploadParameter parameter = (DocumentUploadParameter) SerializationUtils.deserialize((byte[]) message.getPayload());
        
        //transmit through channels via gateway
        Future<DocumentModel> dm = gateway.uploadAsync(parameter);
        logger.info("Gateway tansmitted message and received on replychannel: " + dm.get().toString());
        
        byte[] data = SerializationUtils.serialize(dm.get());
        //MessageProperties properties = MessagePropertiesBuilder.newInstance().setCorrelationId(message.getHeaders().get("amqp_correlationId").toString()).build();
        
        logger.info("Rabbit transmitting message to source...");
        rabbitTemplate.convertAndSend(RabbitMqConfiguration.DOCUMENT_RESPONSE_QUEUE, MessageBuilder.withBody(data).build());
    }
    */
    
    @RabbitListener(queues = RabbitMQConfiguration.DOCUMENTS_NODE_REPLY_QUEUE_NAME)
    public void handleDocumentResponseMessage(Message<?> message) throws MessagingException, InterruptedException, ExecutionException
    {
        if(counter ==0){
            startup = new Date();
        }
        counter ++;
        last = new Date();
        // read message
        //DocumentModel documentModel = (DocumentModel) SerializationUtils.deserialize((byte[]) message.getPayload());        
        //logger.info("Rabbit (client) received message: " + documentModel.toString());                
        logger.info("Rabbit received message with headers: " + message.getHeaders().toString());
        logger.info("Counter: " + counter +  " - Diff: "+(last.getTime()/1000 - startup.getTime()/1000) +" seconds" );
    }    
}
