package joch.integration.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.Objects;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileDocument
{

    private String id;
    
    @NotNull
    private String filename;
        
    @JsonProperty("integrity")
    @NotNull
    private String checksum;  
        
    @NotNull
    @JsonIgnore
    private String storedName;
        
    @NotNull
    @JsonIgnore
    private String path;
        
    @NotNull
    @JsonIgnore
    private String hash;    
        
    @NotNull
    @JsonIgnore
    private final Date createdAt;
        
    @JsonIgnore
    private Date updatedAt;
    
    @JsonIgnore
    private String status;   

    public FileDocument()
    {
        this.createdAt = new Date();
        this.status = "ACTIVE";      
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getStoredName() {
        return storedName;
    }

    public void setStoredName(String storedName) {
        this.storedName = storedName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
        
    public Date getCreatedAt() {
        return createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FileDocument{" + "id=" + id + ", filename=" + filename + ", checksum=" + checksum + ", storedName=" + storedName + ", path=" + path + ", hash=" + hash + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", status=" + status + '}';
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.checksum);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileDocument other = (FileDocument) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.filename, other.filename)) {
            return false;
        }
        return Objects.equals(this.storedName, other.storedName) 
            && Objects.equals(this.id, other.id) 
            && Objects.equals(this.filename, other.filename);
    }         

}
