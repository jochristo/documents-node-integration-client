package joch.integration.model;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author ic
 */
public class DocumentUploadParameter implements Serializable
{
    @NotNull(message = "file is required")
    private MultipartFile file;
    
    @NotNull(message = "integrity is required")
    private String integrity;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getIntegrity() {
        return integrity;
    }

    public void setIntegrity(String integrity) {
        this.integrity = integrity;
    }

    @Override
    public String toString() {
        return "DocumentUploadParameter{" + "file=" + file + ", integrity=" + integrity + '}';
    }
    
    
    
}
