package joch.integration.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author ic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentModel implements Serializable
{
    
    private String id;  
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DocumentModel{" + "id=" + id + '}';
    }
    
    
    
}
