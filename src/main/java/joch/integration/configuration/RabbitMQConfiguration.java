package joch.integration.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Admin
 */
@Configuration
@EnableRabbit
public class RabbitMQConfiguration
{
    public static final String DOCUMENT_REQUEST_EXCHANGE = "documentRequestsExchange";    
    public static final String DOCUMENT_RESPONSE_EXCHANGE = "documentResponseExchange";
    public static final String DOCUMENT_REQUEST_QUEUE = "documentRequestQueue";      
    public static final String DOCUMENT_RESPONSE_QUEUE = "documentResponseQueue";       
    
    public static final String DOCUMENTS_NODE_EXCHANGE_NAME = "app.documents.node";
    public static final String DOCUMENTS_NODE_REQUEST_QUEUE_NAME = "app.documents.request";
    public static final String DOCUMENTS_NODE_REPLY_QUEUE_NAME = "app.documents.reply";
    public static final String DOCUMENTS_NODE_ROUTING_KEY_NAME = "docu";     
    
    @Autowired private ConnectionFactory connectionFactory;    
    @Autowired private RabbitTemplate rabbitTemplate;        
   
    
    @Bean
    public Queue requestQueue() {
        return QueueBuilder.durable(DOCUMENTS_NODE_REQUEST_QUEUE_NAME).build();
    }      
    
    @Bean
    public Queue replyQueue() {
        return QueueBuilder.durable(DOCUMENTS_NODE_REPLY_QUEUE_NAME).build();
    }      
    
    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(DOCUMENTS_NODE_EXCHANGE_NAME);
    }    
 
    @Bean
    public Exchange documentRequestsExchange() {
        return ExchangeBuilder.topicExchange(DOCUMENT_REQUEST_EXCHANGE).build();
    }
 
    @Bean
    public Exchange documentResponseExchange() {
        return ExchangeBuilder.topicExchange(DOCUMENT_RESPONSE_EXCHANGE).build();
    }    
    
    @Bean
    public Binding requestBinding() {
        return BindingBuilder.bind(requestQueue()).to(exchange()).with(DOCUMENTS_NODE_ROUTING_KEY_NAME);
    }

    //@Bean
    public Binding replyBinding() {
        return BindingBuilder.bind(replyQueue()).to(exchange()).with(DOCUMENTS_NODE_ROUTING_KEY_NAME);
    }    
    
    /*
    @Bean
    public Binding bindingOne(Queue documentRequestQueue, TopicExchange documentRequestsExchange) {
        return BindingBuilder.bind(documentRequestQueue).to(documentRequestsExchange).with(DOCUMENT_REQUEST_QUEUE);
    } 
    
    @Bean
    public Binding bindingTwo(Queue documentResponseQueue, TopicExchange documentResponseExchange) {
        return BindingBuilder.bind(documentResponseQueue).to(documentResponseExchange).with(DOCUMENT_RESPONSE_QUEUE);
    }   
    */
    
    @Bean
    public AsyncRabbitTemplate asyncRabbitTemplate() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        //Queue responseQueue = (Queue) context.getBean(DOCUMENT_RESPONSE_QUEUE);
        //return new AsyncRabbitTemplate(rabbitTemplate, container, DOCUMENT_REQUEST_EXCHANGE);
        container.setQueues(replyQueue());        
        return new AsyncRabbitTemplate(rabbitTemplate, container);
        
    }     
        
}
