/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joch.integration.configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import joch.integration.handler.DocumentRequestHandler;
import joch.integration.handler.DocumentResponseHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

/**
 *
 * @author Admin
 */
@Configuration
@EnableIntegration
@IntegrationComponentScan
public class IntegrationConfiguration
{
    
    @Bean
    public ExecutorService cachedThreadPoolExecutorService()
    {        
        // use unbounded thread pool, with automatic thread reclamation
        return Executors.newCachedThreadPool();
    }
    
    @Bean
    public MessageChannel documentConnectorChannel() {
        return new ExecutorChannel(this.cachedThreadPoolExecutorService());
    }  
    
    @Bean
    public MessageChannel documentRequestChannel() {               
        return new ExecutorChannel(this.cachedThreadPoolExecutorService());
    }
    
    @Bean
    public MessageChannel documentReplyChannel() {
        return new ExecutorChannel(this.cachedThreadPoolExecutorService());     
    }    
    
    @Bean
    public MessageChannel documentModelChannel() {
        return new ExecutorChannel(this.cachedThreadPoolExecutorService());     
    }    
    
    @Bean
    public MessageChannel documentErrorChannel() {
        return new ExecutorChannel(this.cachedThreadPoolExecutorService());     
    }     
    
    @Bean
    //@ServiceActivator(inputChannel = "documentRequestChannel", outputChannel = "documentReplyChannel")
    public MessageHandler documentRequestHandler() {
        return new DocumentRequestHandler();
    }  
    
    @Bean
    //@ServiceActivator(inputChannel = "documentReplyChannel")
    public MessageHandler documentResponseHandler() {
        return new DocumentResponseHandler();
    } 
    
}
